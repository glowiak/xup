<img src="http://codeberg.org/glowiak/xup/raw/branch/master/screen.gif">

# xup

XUP (short for indevX UPdater) is a launcher for my mod IndevX. Of course the mod can be downloaded [standalone](https://glowiak.github.io/p/indevx/indevx.jar), and used in any launcher you will, but it is just a nice addition.

### Download

[Get the latest jar](https://glowiak.github.io/p/indevx/xup.jar)

### Technology used

The launcher is written in Java, with no external libraries used.

### Roadmap

- [X] game installing and launching
- [X] file download screen
- [X] error handling
- [X] replace old official textures with my own ones
- [] add M$ authentication
- [X] add graphical options screen

### Development and building

This repository is an eclipse project, clone it into your workspace and voila.

To build the jar use the build.sh script:

`
./build.sh
`

### FAQ

- The Launcher is slow

`
If you experience the slowness, it is because of the splash code. The splashes need a lot of calculation to display properly. I will definitely replace this with better code in the future, but for now just disable widgets in options. A note that this would also make the jumping items freeze.
`

### Credits

All the launcher code is written by myself, <strike>the textures are extracted from the old 2010 launcher (the dirt.png), or from the Indev jarfile (the MINECRAFT logo), though I am going to replace them all to avoid potential copyright issues.</strike> I have replaced the textures with my ones. finally.

This is unapproved by Mojang or Microsoft, just my project.
