#!/bin/sh

set -e

export CWD=$(pwd)

rm -rf $CWD/dest
mkdir -pv $CWD/dest

cp -rfv resources/* $CWD/dest/
cp -rfv src/* $CWD/dest/

cd $CWD/dest

javac `find . -name '*.java'`
jar cvfe $CWD/xup.jar com.glowiak.xup.XupLauncherStarter `find . -name '*.class'` `find . -name '*.png'`

cd $CWD

rm -rf $CWD/dest
