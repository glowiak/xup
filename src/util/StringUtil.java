package util;

public class StringUtil
{
	public static String getUrlFilename(String url)
	{
		return url.substring(url.lastIndexOf('/') + 1, url.length());
	}
	public static String setCharAt(int index, char c, String src)
	{
		if (index > src.length())
		{
			return src;
		}
		return src.substring(0, index) + c + src.substring(index + 1);
	}
	public static String setEmptyCharAt(int index, String src)
	{
		if (index > src.length())
		{
			return src;
		}
		return src.substring(0, index) + src.substring(index + 1);
	}
}
