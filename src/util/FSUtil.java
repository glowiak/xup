package util;

import java.io.File;

public class FSUtil
{
	public static final void rmdir(File dir)
	{
		if (dir.isDirectory())
		{
			for (File f : dir.listFiles())
			{
				rmdir(f);
			}
			dir.delete();
		} else
		{
			dir.delete();
		}
	}
	public static final File whereis(File f)
	{
		String s = f.getPath();
		
		System.out.println(s);
		
		if (!s.contains("/") || !s.contains("" + File.separator))
			return null;
		
		File fa = new File(s.substring(0, s.lastIndexOf(File.separator)));
		System.out.println(fa.getPath());
		return fa;
	}
}
