package util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.net.URL;

public class FetchUtil
{
	public static boolean OVERRIDE_IF_EXISTS = false;
	
	public static void fetch(String src, String dst) throws IOException
	{
		File fs = new File(dst);
		if (!fs.exists() || OVERRIDE_IF_EXISTS)
		{
			if (OVERRIDE_IF_EXISTS)
			{
				fs.delete();
			}
			URL u = new URL(src);
			Path p = Paths.get(dst);
			InputStream is = u.openStream();
			Files.copy(is, p, StandardCopyOption.REPLACE_EXISTING);
		}
	}
}
