package util;

public class OSUtil
{
	public static final byte OS_WINDOWS = 0;
	public static final byte OS_LINUX = 1;
	public static final byte OS_MAC = 2;
	public static final byte OS_SOLARIS = 3;
	public static final byte OS_OTHER = 4;
	
	public static byte getOS()
	{
		byte w = OS_OTHER;
		
		String s = System.getProperty("os.name").toLowerCase();
		if (s.contains("win"))
		{
			w = OS_WINDOWS;
		} else if (s.contains("lin"))
		{
			w = OS_LINUX;
		} else if (s.contains("mac") || s.contains("osx"))
		{
			w = OS_MAC;
		} else if (s.contains("sol"))
		{
			w = OS_SOLARIS;
		}
		
		return w;
	}
	public static char getSeparator()
	{
		char w = ':';
		
		if (getOS() == OS_WINDOWS)
		{
			w = ';'; // only windows is so dumb to use a different separator
		}
		
		return w;
	}
}
