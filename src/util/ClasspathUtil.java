package util;

import java.io.File;

public class ClasspathUtil
{
	public static String generateClasspath(File[] cp)
	{
		String w = "";
		
		for (int i = 0; i < cp.length; i++)
		{
			w += cp[i];
			w += OSUtil.getSeparator();
		}
		if (w.charAt(w.length() - 1) == OSUtil.getSeparator())
		{
			w = StringUtil.setEmptyCharAt(w.length() - 1, w);
		}
		
		return w;
	}
	public static String getRunningJvm()
	{
		String s =  System.getProperty("java.home") + "/bin/java";
		if (OSUtil.getOS() == OSUtil.OS_WINDOWS)
			s += ".exe";
		
		return s;
	}
}
