package util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtil
{
	private static final int BUFFER_SIZE = 4096;
	
	public static void unzip(String zipfile, String destdir) throws IOException
	{
		File dest = new File(destdir);
		if (!dest.exists())
			dest.mkdirs();
		
		ZipInputStream zis = new ZipInputStream(new FileInputStream(zipfile));
		ZipEntry ze = zis.getNextEntry();
		
		while (ze != null)
		{
			String fp = destdir + File.separator + ze.getName();
			if (ze.isDirectory())
			{
				new File(fp).mkdirs();
			} else
			{
				_extractFile(zis, fp);
			}
			zis.closeEntry();
			ze = zis.getNextEntry();
		}
		zis.close();
	}
	private static final void _extractFile(ZipInputStream zis, String path) throws IOException
	{
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(path));
		} catch (FileNotFoundException fnfe)
		{
			// an ugly fix that works
			
			new File(path).mkdirs();
			new File(path).delete();
			bos = new BufferedOutputStream(new FileOutputStream(path));
		}
		
		byte[] bytes = new byte[BUFFER_SIZE];
		int rb = 0;
		while ((rb = zis.read(bytes)) != -1)
		{
			bos.write(bytes, 0, rb);
		}
		
		bos.close();
	}
}
