package com.glowiak.xup.launcher;

import util.OSUtil;
import util.StringUtil;
import util.ClasspathUtil;
import util.FSUtil;
import util.ZipUtil;
import util.FetchUtil;

import com.glowiak.xup.launcher.event.LauncherEvent;
import com.glowiak.xup.ui.XupDownloadPanel;

import java.util.ArrayList;

import java.io.File;
import java.io.IOException;

public class Launcher implements Runnable
{
	public LauncherData data;
	public Assets gameAssets;
	public ReadableData rdata;
	public LauncherOptions opt;
	public ArrayList<LauncherEvent> gameEvents;
	public XupDownloadPanel _optional_dp;
	public boolean retval;
	
	public Launcher()
	{
		this.data = new LauncherData();
		this.rdata = new ReadableData();
		this.opt = new LauncherOptions();
		this.gameEvents = new ArrayList<LauncherEvent>();
		
		this.gameAssets = new Assets(this);
		this.gameAssets.mainJar = "https://github.com/glowiak/p/raw/master/indevx/indevx.jar";
		this.gameAssets.libjar.add("https://github.com/glowiak/p/raw/master/indevx/lwjgl.jar");
		this.gameAssets.libjar.add("https://github.com/glowiak/p/raw/master/indevx/lwjgl_util.jar");
		this.gameAssets.libjar.add("https://github.com/glowiak/p/raw/master/indevx/jinput.jar");
		this.gameAssets.libjar.add("https://libraries.minecraft.net/net/minecraft/launchwrapper/1.6/launchwrapper-1.6.jar");
		this.gameAssets.libjar.add("https://libraries.minecraft.net/net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar");
		this.gameAssets.libjar.add("https://libraries.minecraft.net/org/ow2/asm/asm-all/4.1/asm-all-4.1.jar");
		this.gameAssets.natives.put(OSUtil.OS_WINDOWS, "https://github.com/glowiak/p/raw/master/indevx/windows_natives.jar");
		this.gameAssets.natives.put(OSUtil.OS_LINUX, "https://github.com/glowiak/p/raw/master/indevx/linux_natives.jar");
		this.gameAssets.natives.put(OSUtil.OS_MAC, "https://github.com/glowiak/p/raw/master/indevx/macosx_natives.jar");
		this.gameAssets.natives.put(OSUtil.OS_SOLARIS, "https://github.com/glowiak/p/raw/master/indevx/solaris_natives.jar");
		
		if (this.data.rdp.exists())
		{
			try {
				this.rdata.load(this.data.rdp);
			} catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
		}
		if (this.data.optPath.exists())
		{
			try {
				this.opt.load(this.data.optPath);
			} catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
		} else
		{
			try {
				this.opt.save(this.data.optPath);
			} catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
		}
		this.retval = true;
	}
	public final void cleanEvents()
	{
		this.gameEvents = new ArrayList<LauncherEvent>();
	}
	public final void addEvent(LauncherEvent e)
	{
		this.gameEvents.add(e);
	}
	public final void setDownloadPanel(XupDownloadPanel dp)
	{
		this._optional_dp = dp;
		this.gameAssets._optional_dp = dp;
	}
	protected final boolean hasDP()
	{
		return this._optional_dp != null;
	}
	public final void run()
	{
		this.retval = true;
		
		if ((Boolean)this.opt.getValue("force_update"))
			FetchUtil.OVERRIDE_IF_EXISTS = true;
		
		if (!this.gameAssets.fetch())
		{
			this.retval = false;
			return;
		}
		FetchUtil.OVERRIDE_IF_EXISTS = false;
		if (this.hasDP())
		{
			this._optional_dp.displayString = "Building classpath";
			this._optional_dp.repaint();
			this._optional_dp.parent.repaint();
		}
		
		ArrayList<File> fa = new ArrayList<File>();
		for (int i = 0; i < this.gameAssets.libjar.size(); i++)
		{
			String s = this.data.libDir.getPath() + "/";
			s += StringUtil.getUrlFilename(this.gameAssets.libjar.get(i));
			
			fa.add(new File(s));
		}
		fa.add(new File(this.data.natDir.getPath() + "/natives.jar"));
		fa.add(new File(this.data.binDir.getPath() + "/minecraft.jar"));
		
		File[] faa = new File[fa.size()];
		faa = fa.toArray(faa);
		
		String cp = ClasspathUtil.generateClasspath(faa);
		
		if (this.hasDP())
		{
			this._optional_dp.displayString = "Extracting natives";
			this._optional_dp.repaint();
			this._optional_dp.parent.repaint();
		}
		if (this.data.tnatDir.exists())
		{
			FSUtil.rmdir(this.data.tnatDir);
		}
		this.data.tnatDir.mkdirs();
		try {
			ZipUtil.unzip(new File(this.data.natDir.getPath() + "/natives.jar").getPath(), this.data.tnatDir.getPath());
		} catch (IOException ioe)
		{
			ioe.printStackTrace();
			this.retval = false;
			return;
		}
		if (this.hasDP())
		{
			this._optional_dp.displayString = "Running IndevX";
			this._optional_dp.repaint();
			this._optional_dp.parent.repaint();
		}
		
		GameInstance gi = new GameInstance(this);
		
		gi.classpath = cp;
		gi.javaPath = (String)this.opt.getValue("jvm");
		gi.javaArgs = "-Xmx" + (Integer)this.opt.getValue("xmxMb") + "M";
		gi.javaArgs += " -Xms" + (Integer)this.opt.getValue("xmsMb") + "M";
		gi.javaArgs += " -Xmn" + (Integer)this.opt.getValue("xmnMb") + "M";
		gi.javaArgs += " -Djava.util.Arrays.useLegacyMergeSort=true";
		gi.javaArgs += " -Dhttp.proxyHost=betacraft.uk";
		gi.javaArgs += " -Djava.library.path=" + this.data.tnatDir.getPath();
		gi.mainClass = "net.minecraft.launchwrapper.Launch";
		gi.gameArgs = "--tweakClass net.minecraft.launchwrapper.AlphaVanillaTweaker ";
		gi.gameArgs += this.rdata.lastUsername;
		gi.events = this.gameEvents;
		
		if (!gi.check())
		{
			this.retval = false;
			return;
		}
		if ((Boolean)this.opt.getValue("debug"))
		{
			System.out.println(gi.makeCommand());
		}
		
		gi.launch();
	}
	public final boolean force_save_opt()
	{
		if (this.data.optPath.exists())
		{
			this.data.optPath.delete();
		}
		try {
			this.opt.save(this.data.optPath);
		} catch (IOException ioe)
		{
			ioe.printStackTrace();
			
			return false;
		}
		
		return true;
	}
	public final void launch()
	{
		new Thread(this).start();
	}
}
