package com.glowiak.xup.launcher.event;

import com.glowiak.xup.launcher.GameInstance;

public class LauncherEvent
{
	public static final byte POST_EXIT = 0;
	public static final byte PRE_LAUNCH = 1;
	
	public GameInstance game;
	public String name;
	public byte type;
	
	public LauncherEvent(String s, byte type)
	{
		this.name = s;
		this.type = type;
	}
	
	public void run()
	{
	}
}
