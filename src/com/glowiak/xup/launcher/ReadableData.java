package com.glowiak.xup.launcher;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;

public class ReadableData
{
	public String lastUsername;
	
	public ReadableData()
	{
		this.lastUsername = "";
	}
	public final void save(File f) throws IOException
	{
		FileWriter fw = new FileWriter(f);
		
		fw.write("username:" + this.lastUsername + "\n");
		
		fw.close();
	}
	public final void load(File f) throws IOException
	{
		if (!f.exists())
		{
			throw new IOException("File does not exist!");
		}
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		
		String s = null;
		while ((s = br.readLine()) != null)
		{
			if (s.startsWith("username:"))
			{
				this.lastUsername = s.replace("username:", "");
			}
		}
		
		br.close();
		fr.close();
	}
}
