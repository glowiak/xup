package com.glowiak.xup.launcher;

import util.OSUtil;
import java.io.File;

public class LauncherData
{
	public File mcDir;
	public File libDir;
	public File binDir;
	public File natDir;
	public File rdp;
	public File optPath;
	public File tnatDir;
	public File saveDir;
	
	public LauncherData()
	{
		String s = "";
		s = new File(".").getPath() + "/minecraft";
		byte os = OSUtil.getOS();
		if (os == OSUtil.OS_LINUX)
		{
			s = System.getProperty("user.home") + "/.minecraft";
		} else if (os == OSUtil.OS_WINDOWS)
		{
			s = System.getenv("appdata") + "/.minecraft";
		} else if (os == OSUtil.OS_MAC)
		{
			s = System.getProperty("user.home", ".") + "Library/Application Support/minecraft";
		}
		this.mcDir = new File(s);
		if (!this.mcDir.exists())
		{
			this.mcDir.mkdirs();
		}
		this.libDir = new File(this.mcDir.getPath() + "/lib");
		this.binDir = new File(this.mcDir.getPath() + "/bin");
		this.natDir = new File(this.mcDir.getPath() + "/natives");
		if (!this.libDir.exists())
		{
			this.libDir.mkdirs();
		}
		if (!this.binDir.exists())
		{
			this.binDir.mkdirs();
		}
		if (!this.natDir.exists())
		{
			this.natDir.mkdirs();
		}
		this.rdp = new File(this.mcDir.getPath() + "/xup_prop.txt");
		this.optPath = new File(this.mcDir.getPath() + "/optx.txt");
		this.tnatDir = new File(this.mcDir.getPath() + "/_nat");
		this.saveDir = new File(this.mcDir.getPath() + "/saves");
	}
}
