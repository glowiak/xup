package com.glowiak.xup.launcher;

import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import util.ClasspathUtil;

@SuppressWarnings("rawtypes")
public class LauncherOptions
{
	public Object[][] options; // format: { name(STRING), type(CLASS), val(TYPE), displayName(STRING), isPath(BOOLEAN, additional to string only) }
	
	public LauncherOptions()
	{
		this.setDefaults();
	}
	private final void setDefaults()
	{
		
		this.options = new Object[][]
		{
			{
				"dispose_after_start",
				Boolean.class,
				true,
				"Hide after start"
			},
			{
				"xmxMb",
				Integer.class,
				1024,
				"Maximum memory (MB)"
			},
			{
				"xmnMb",
				Integer.class,
				1024,
				"Minimum memory (MB)"
			},
			{
				"xmsMb",
				Integer.class,
				1024,
				"Minimum heap (MB)"
			},
			{
				"jvm",
				String.class,
				ClasspathUtil.getRunningJvm(),
				"Java path",
				true
			},
			{
				"close_after_start",
				Boolean.class,
				false,
				"Close after start"
			},
			{
				"force_update",
				Boolean.class,
				false,
				"Force update"
			},
			{
				"debug",
				Boolean.class,
				false,
				"Debug features"
			},
			{
				"widgets",
				Boolean.class,
				true,
				"Animated widgets"
			}
		};
	}
	public final boolean hasOption(String name)
	{
		for (int i = 0; i < this.options.length; i++)
		{
			String s = (String)this.options[i][0];
			if (s.equals(name))
				return true;
		}
		
		return false;
	}
	public final Class getType(String name)
	{
		for (int i = 0; i < this.options.length; i++)
		{
			String s = (String)this.options[i][0];
			if (s.equals(name))
				return (Class)this.options[i][1];
		}
		
		return null;
	}
	public final Object getValue(String name)
	{
		for (int i = 0; i < this.options.length; i++)
		{
			String s = (String)this.options[i][0];
			if (s.equals(name))
				return this.options[i][2];
		}
		
		return null;
	}
	public final Object[] getOption(String name)
	{
		for (int i = 0; i < this.options.length; i++)
		{
			String s = (String)this.options[i][0];
			if (s.equals(name))
				return this.options[i];
		}
		
		return null;
	}
	public final void save(File path) throws IOException
	{
		FileWriter fw = new FileWriter(path);
		
		for (int i = 0; i < this.options.length; i++)
		{
			fw.write(this.options[i][0] + ":");

			Class clazz = (Class)this.options[i][1];
			if (clazz == String.class)
				fw.write((String)this.options[i][2]);
			else if (clazz == Integer.class)
				fw.write("" + (Integer)this.options[i][2]);
			else if (clazz == Boolean.class)
				fw.write(Boolean.toString((Boolean)this.options[i][2]));
			else
			{
			}
			fw.write("\n");
		}
		
		fw.close();
	}
	public final void load(File path) throws IOException
	{
		FileReader fr = new FileReader(path);
		BufferedReader br = new BufferedReader(fr);
		
		String s = null;
		while ((s = br.readLine()) != null)
		{
			for (int i = 0; i < this.options.length; i++)
			{
				String on = (String)this.options[i][0] + ":";
				if (s.startsWith(on))
				{
					Class clazz = (Class)this.options[i][1];
					
					if (clazz == String.class)
						this.options[i][2] = s.replace(on, "");
					else if (clazz == Integer.class)
						this.options[i][2] = Integer.parseInt(s.replace(on, ""));
					else if (clazz == Boolean.class)
						this.options[i][2] = Boolean.parseBoolean(s.replace(on, ""));
					else
					{
					}
				}
			}
		}
		
		br.close();
		fr.close();
	}
}
