package com.glowiak.xup.launcher;

public class LauncherBrand
{
	public static final String WINDOW_TITLE = "xup";
	public static final String LAUNCHER_NAME = "xup";
	public static final String LAUNCHER_VERSION = "0.2.1f";
	public static final String[] CREDITS = new String[]
	{
		"Code: glowiak",
		"Textures: glowiak"
	};
}
