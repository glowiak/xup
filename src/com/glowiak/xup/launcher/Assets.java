package com.glowiak.xup.launcher;

import util.FetchUtil;
import util.StringUtil;
import util.OSUtil;

import com.glowiak.xup.ui.XupDownloadPanel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.io.IOException;

public class Assets
{
	private Launcher l;
	public ArrayList<String> libjar;
	public LinkedHashMap<Byte, String> natives;
	public String mainJar;
	public XupDownloadPanel _optional_dp;
	
	public Assets(Launcher l)
	{
		this.l = l;
		this.libjar = new ArrayList<String>();
		this.natives = new LinkedHashMap<Byte, String>();
	}
	protected final boolean fetch()
	{
		boolean w = true;
		
		for (int i = 0; i < this.libjar.size(); i++)
		{
			if (this.l.hasDP())
			{
				this._optional_dp.displayString = "Downloading ";
				this._optional_dp.displayString += StringUtil.getUrlFilename(this.libjar.get(i));
				this._optional_dp.repaint();
			}
			
			String dst = this.l.data.libDir + "/" + StringUtil.getUrlFilename(this.libjar.get(i));
			if ((Boolean)this.l.opt.getValue("debug"))
			{
				System.out.println("Fetch " + this.libjar.get(i) + " to " + dst);
			}
			try {
				FetchUtil.fetch(this.libjar.get(i), dst);
			} catch (IOException ioe)
			{
				ioe.printStackTrace();
				w = false;
				break;
			}
		}
		String dst = this.l.data.binDir + "/minecraft.jar";
		try {
			FetchUtil.fetch(this.mainJar, dst);
		} catch (IOException ioe)
		{
			ioe.printStackTrace();
			w = false;
		}
		dst = this.l.data.natDir + "/natives.jar";
		try {
			FetchUtil.fetch(this.natives.get(OSUtil.getOS()), dst);
		} catch (IOException ioe)
		{
			ioe.printStackTrace();
			w = false;
		} catch (NullPointerException npe)
		{
			npe.printStackTrace();
			w = false;
		}
		
		return w;
	}
}
