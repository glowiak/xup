package com.glowiak.xup.launcher;

import com.glowiak.xup.launcher.Launcher;
import com.glowiak.xup.launcher.event.LauncherEvent;

import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.util.ArrayList;

public class GameInstance implements Runnable
{
	private Launcher l;
	
	public String mainClass;
	public String classpath;
	public String javaPath;
	public String javaArgs;
	public String gameArgs;
	
	public String ERROR_TEXT;
	public ArrayList<LauncherEvent> events;
	
	public GameInstance(Launcher l)
	{
		this.l = l;
		
		this.mainClass = "";
		this.classpath = "";
		this.javaPath = "";
		this.javaArgs = "";
		this.gameArgs = "";
		
		this.ERROR_TEXT = "";
		this.events = new ArrayList<LauncherEvent>();
	}
	public boolean check()
	{
		if (!new File(javaPath).exists())
		{
			this.ERROR_TEXT = "Java path does not exist";
			return false;
		}
		
		return true;
	}
	public String makeCommand()
	{
		String s = this.javaPath + " ";
		
		s += this.javaArgs;
		s += " -cp ";
		s += this.classpath + " ";
		s += this.mainClass + " ";
		s += this.gameArgs;
		
		return s;
	}
	public void run()
	{
		try {
			for (int i = 0; i < this.events.size(); i++)
			{
				if (this.events.get(i).type == LauncherEvent.PRE_LAUNCH)
				{
					if ((Boolean)this.l.opt.getValue("debug"))
					{
						System.out.println("Running pre-run event: " + this.events.get(i).name);
					}
					this.events.get(i).run();
				}
			}
			
			Process p = Runtime.getRuntime().exec(this.makeCommand());
			
			InputStreamReader isr = new InputStreamReader(p.getInputStream());
			BufferedReader br_log = new BufferedReader(isr);
			String s = null;
			while ((s = br_log.readLine()) != null)
			{
				System.out.println(s);
			}
			InputStreamReader isr_err = new InputStreamReader(p.getErrorStream());
			BufferedReader br_err = new BufferedReader(isr_err);
			s = null;
			while ((s = br_err.readLine()) != null)
			{
				System.out.println(s);
			}
			br_log.close();
			br_err.close();
			
			for (int i = 0; i < this.events.size(); i++)
			{
				if (this.events.get(i).type == LauncherEvent.POST_EXIT)
				{
					if ((Boolean)this.l.opt.getValue("debug"))
					{
						System.out.println("Running game-exit event: " + this.events.get(i).name);
					}
					this.events.get(i).run();
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public void launch()
	{
		new Thread(this).start();
	}
}
