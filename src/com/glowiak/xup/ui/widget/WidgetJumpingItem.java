package com.glowiak.xup.ui.widget;

import com.glowiak.xup.ui.ResizablePanel;

import javax.imageio.ImageIO;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.Random;

public class WidgetJumpingItem extends Widget
{
	private static final int TX_WIDTH = 32;
	private static final int TX_HEIGHT = 32;
	private static final Random rand = new Random();
	
	private float x;
	private float y;
	private Image[] images;
	private int imageSlot;
	private float speed;
	private int min_y;
	private boolean fup;
	
	public WidgetJumpingItem(ResizablePanel p)
	{
		super(p);
		
		this.images = new Image[4];
		
		for (int i = 0; i < this.images.length; i++)
		{
			try {
				this.images[i] = ImageIO.read(this.getClass().getResource("/item" + i + ".png"));
				this.images[i] = this.images[i].getScaledInstance(TX_WIDTH, TX_HEIGHT, Image.SCALE_DEFAULT);
			} catch (IOException ioe)
			{
				ioe.printStackTrace();
				System.out.println("A launcher asset is missing or damaged!");
				System.exit(1);
			}
		}
		
		this.y = 0;
		this.x = 0;
		this.imageSlot = 0;
		this.speed = -1;
		this.min_y = 0;
		this.fup = true;
		
		this.rebase();
	}
	public void onResize()
	{
		this.rebase();
	}
	public void update()
	{
		if ((int)this.y < this.min_y + 150)
			this.fup = false;
		if ((int)this.y > this.panel.getHeight() + TX_HEIGHT)
		{
			this.rebase();
			this.fup = true;
		}
		if (this.fup)
		{
			this.speed -= 0.2f;
		} else
		{
			this.speed += 0.2f;
		}
		this.y += this.speed;
	}
	public void paint(Graphics g)
	{
		g.drawImage(this.images[this.imageSlot], (int)this.x, (int)this.y, null);
	}
	private final void rebase()
	{
		if (this.panel.getWidth() > TX_WIDTH)
			this.x = rand.nextInt(this.panel.getWidth() - TX_WIDTH);
		this.y = this.panel.getHeight() + TX_HEIGHT;
		this.imageSlot = rand.nextInt(images.length);
		this.min_y = this.panel.getHeight() / 2 + rand.nextInt(30);
	}
}
