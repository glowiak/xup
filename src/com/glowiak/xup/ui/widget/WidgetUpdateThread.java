package com.glowiak.xup.ui.widget;

import com.glowiak.xup.launcher.Launcher;
import java.util.ArrayList;

public class WidgetUpdateThread implements Runnable
{
	public static WidgetUpdateThread instance;
	private static final long SECOND = 1000000000L;
	
	protected ArrayList<Widget> updateList;
	private int tps;
	private Launcher l;
	private boolean finished;
	
	public WidgetUpdateThread(int tps, Launcher l)
	{
		this.l = l;
		this.updateList = new ArrayList<Widget>();
		this.tps = tps;
		this.finished = false;
		
		instance = this;
	}
	public void onOptionsClose()
	{
		if ((Boolean)this.l.opt.getValue("widgets") && this.finished)
		{
			new Thread(this).start();
		}
	}
	public void run()
	{
		final double tickInterval = SECOND/this.tps;
		long lastTime = System.nanoTime();
		double unprocessed = 0.0d;
		double nextTickTime = lastTime + tickInterval;
		
		while ((Boolean)this.l.opt.getValue("widgets"))
		{
			long currentTime = System.nanoTime();
			long passedTime = currentTime - lastTime;
			double remainingTime = passedTime / tickInterval;
			unprocessed += remainingTime;
			
			while (unprocessed >= 1)
			{
				unprocessed--;
				
				for (int i = 0; i < this.updateList.size(); i++)
				{
					this.updateList.get(i).update();
				}
			}
			lastTime = currentTime;
			nextTickTime += tickInterval;
			
			if (remainingTime < 0)
				remainingTime = 0;
			
			try {
				Thread.sleep((long)remainingTime);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		this.finished = true;
	}
}
