package com.glowiak.xup.ui.widget;

import com.glowiak.xup.ui.ResizablePanel;

import java.awt.Graphics;

public class Widget
{
	protected ResizablePanel panel;
	
	public Widget(ResizablePanel p)
	{
		this.panel = p;
		
		WidgetUpdateThread.instance.updateList.add(this);
	}
	public void onResize()
	{
	}
	public void update()
	{
	}
	public void paint(Graphics g)
	{
	}
}
