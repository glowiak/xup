package com.glowiak.xup.ui.widget;

import com.glowiak.xup.ui.ResizablePanel;

import java.awt.Graphics;
import java.awt.Font;
import java.awt.Color;
import java.awt.Image;

import java.awt.image.BufferedImage;
import java.util.Random;

public class WidgetSplash extends Widget
{
	private static final float MIN_FSIZE = 1.0f;
	private static final float MAX_FSIZE = 1.4f;
	private static final Random rand = new Random();
	private static final String[] splashes = new String[]
	{
			"Made by glowiak!",
			"The work of glowiak!",
			"Random splash!",
			"First class!",
			"Classy design!",
			"Manual resizing!",
			"0.20zł!",
			"That's a moon!",
			"You underestimate my power!",
			"For a just and noble Cause!",
			"Frequently rotating!",
			"Game of Notch!",
			"Doublely annoying!",
			"Doublely annoying!",
			"h₂(e)nh₂t!",
			"Impass from the hide!",
			"Far Lands!",
			"Boats!",
			"Cogito ergo sum!",
			"Legacy manufacturing!",
			"Coöperative engineering!",
			"1 + 1 = 5!",
			"War is peace!",
			"Freedom is slavery!",
			"Ignorance is strength!",
			"Jack Sparrow!",
			"Just pirate it!",
			"Expanded by default!",
			"Big Brother is watching you!",
			"Premium!",
			"As seen on TV!",
			"Count Dooku was right!",
			"Jedi serve the corrupt Senate!",
			"171!",
			"S30+!",
			"An evil lair!",
			"Layers of the corner!",
			"Keep it simple!",
			"Install Gentoo!"
	};
	
	private int x;
	private int y;
	private String text;
	private float fontSize;
	private float changeBy;
	private BufferedImage textImg;
	
	public WidgetSplash(ResizablePanel p)
	{
		super(p);
		
		this.x = 0;
		this.y = 0;
		this.text = splashes[rand.nextInt(splashes.length)];
		this.fontSize = 1.0f;
		this.changeBy = 0.02f;
		
		this.generateImage();
	}
	public final void setPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	private final void generateImage()
	{
		int iw = this.text.length() * 15;
		int ih = 25;
		
		this.textImg = new BufferedImage(iw, ih, BufferedImage.TYPE_INT_ARGB);
		Graphics g = this.textImg.getGraphics();
		
		g.setColor(Color.yellow);
		g.drawString(this.text, 10, 10);
	}
	public void update()
	{
		this.fontSize += changeBy;
		
		if (this.fontSize > MAX_FSIZE)
			this.changeBy = -this.changeBy;
		
		if (this.fontSize < MIN_FSIZE)
			this.changeBy = -this.changeBy;
		
		this.panel.repaint();
	}
	public void paint(Graphics g)
	{
		Image toDraw = ((Image)this.textImg).getScaledInstance((int)(this.textImg.getWidth(null) * this.fontSize), (int)(this.textImg.getHeight(null) * this.fontSize), Image.SCALE_DEFAULT);
		
		g.drawImage(toDraw, this.x, this.y, null);
	}
}
