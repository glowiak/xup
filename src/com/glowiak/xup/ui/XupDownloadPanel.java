package com.glowiak.xup.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;
import java.awt.Font;

@SuppressWarnings("serial")
public class XupDownloadPanel extends ResizablePanel
{
	public String displayString;
	
	public XupDownloadPanel(XupWindow parent)
	{
		super(parent);
		
		this.setPreferredSize(this.parent.defaultSize);
		this.setDoubleBuffered(true);
		this.setLayout(null);
		
		this.displayString = "Component initialization";
	}
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Image dirt = this.parent.p_login.images[0];
		
		int tx = (int)(this.getWidth() / 32) + 2;
		int ty = (int)(this.getHeight() / 32) + 2;
		
		for (int i = 0; i < tx; i++)
		{
			for (int j = 0; j < ty; j++)
			{
				g.drawImage(dirt, i * 32, j * 32, null);
			}
		}
		
		g.setColor(Color.white);
		
		Font oldf = g.getFont();
		Font newf = oldf.deriveFont(20f);
		Font titlef = oldf.deriveFont(50f);
		
		g.setFont(titlef);
		g.drawString("Updating  IndevX", this.getWidth() / 2 - (int)((80 * "Updating  IndevX".length()) / 7), this.getHeight() / 2 - 50);
		g.setFont(newf);
		g.drawString(this.displayString, this.getWidth() / 2 - (int)((80 * this.displayString.length()) / 15), this.getHeight() / 2 + 10);
		g.setFont(oldf);
	}
	public final void updateOnResize()
	{
	}
}
