package com.glowiak.xup.ui;

import javax.swing.JOptionPane;

import javax.imageio.ImageIO;
import java.util.Random;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.glowiak.xup.launcher.LauncherBrand;
import com.glowiak.xup.launcher.event.LauncherEvent;

import com.glowiak.xup.ui.widget.WidgetSplash;
import com.glowiak.xup.ui.widget.WidgetJumpingItem;

@SuppressWarnings("serial")
public class XupLoginPanel extends ResizablePanel
{
	private static final Random rand = new Random();
	
	protected Image[] images;
	protected JTextField tf_username;
	protected JButton b_login;
	protected JButton b_options;
	
	public XupLoginPanel(XupWindow p)
	{
		super(p);
		
		this.setPreferredSize(this.parent.defaultSize);
		this.setLayout(null);
		
		this.loadImages();
		this.initComponents();
		this.updateOnResize();
	}
	private final void loadImages()
	{
		this.images = new Image[5];
		
		try {
			this.images[0] = ImageIO.read(this.getClass().getResource("/dirt.png"));
			this.images[0] = this.images[0].getScaledInstance(32, 32, Image.SCALE_DEFAULT);
			
			this.images[1] = ImageIO.read(this.getClass().getResource("/logo.png"));
			this.images[1] = this.images[1].getScaledInstance(512, 512, Image.SCALE_DEFAULT);
			
			this.images[2] = ImageIO.read(this.getClass().getResource("/favicon.png"));
		} catch (IOException ioe)
		{
			ioe.printStackTrace();
			System.exit(1);
		}
	}
	private final void initComponents()
	{
		this.tf_username = new JTextField();
		this.b_login = new JButton("Play");
		this.b_options = new JButton("Options");
		
		if (!parent.launcher.rdata.lastUsername.isEmpty())
		{
			this.tf_username.setText(parent.launcher.rdata.lastUsername);
		}
		
		this.b_login.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (!tf_username.getText().isEmpty())
				{
					parent.launcher.rdata.lastUsername = tf_username.getText();
					try {
						parent.launcher.rdata.save(parent.launcher.data.rdp);
					} catch (IOException ioe)
					{
						ioe.printStackTrace();
					}
					
					// insert username to the file
					try {
						File fi = new File(parent.launcher.data.saveDir.getPath() + "/indevx_profile.dat");
						FileWriter fw = new FileWriter(fi);
						
						fw.write(parent.launcher.rdata.lastUsername + "\n");
						
						int sessionId = rand.nextInt(100000);
						fw.write("" + sessionId + "\n");
						
						fw.close();
					} catch (IOException ioe)
					{
						ioe.printStackTrace();
						System.out.println("Failed to insert username");
					}
					
					parent.launcher.cleanEvents();
					if ((Boolean)parent.launcher.opt.getValue("dispose_after_start"))
					{
						parent.launcher.addEvent(new LauncherEvent("Close window", LauncherEvent.PRE_LAUNCH)
						{
							public void run()
							{
								parent.setVisible(false);
							}
						});
						parent.launcher.addEvent(new LauncherEvent("Reactivate window", LauncherEvent.POST_EXIT)
						{
							public void run()
							{
								try {
									parent.setPanel(0);
								} catch (IndexOutOfBoundsException e)
								{
									e.printStackTrace();
								}
								parent.setVisible(true);
							}
						});
					}
					if ((Boolean)parent.launcher.opt.getValue("close_after_start"))
					{
						parent.launcher.addEvent(new LauncherEvent("Close window", LauncherEvent.PRE_LAUNCH)
						{
							public void run()
							{
								parent.setVisible(false);
								parent.dispose();
							}
						});
						parent.launcher.addEvent(new LauncherEvent("Close launcher", LauncherEvent.POST_EXIT)
						{
							public void run()
							{
								System.exit(0);
							}
						});
					}
					if (!(Boolean)parent.launcher.opt.getValue("dispose_after_start") &&
							!(Boolean)parent.launcher.opt.getValue("close_after_start"))
					{
						parent.launcher.addEvent(new LauncherEvent("Revert panel", LauncherEvent.POST_EXIT)
						{
							public void run()
							{
								try {
									parent.setPanel(0);
								} catch (IndexOutOfBoundsException e)
								{
									e.printStackTrace();
								}
							}
						});
					}
					
					try {
						parent.setPanel(2);
					} catch (IndexOutOfBoundsException ee)
					{
						ee.printStackTrace();
					}
					parent.repaint();
					parent.launcher.setDownloadPanel(parent.p_download);
					
					parent.launcher.launch();
					boolean f = parent.launcher.retval;
					
					if (!f)
					{
						String msg = "An error occured. Look into the terminal for details.";
						JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		this.b_options.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					parent.setPanel(1);
				} catch (IndexOutOfBoundsException ioobe)
				{
					ioobe.printStackTrace();
				}
			}
		});
		
		this.add(this.tf_username);
		this.add(this.b_login);
		this.add(this.b_options);
		
		this.widgets.add(new WidgetSplash(this));
		for (int i = 0; i < rand.nextInt(5) + 3; i++)
		{
			this.widgets.add(new WidgetJumpingItem(this));
		}
	}
	protected final void updateOnResize()
	{
		int hy = this.getHeight() / 2 - 80;
		
		this.tf_username.setBounds(this.getWidth() / 2 - 60, hy, 120, 25);
		this.b_login.setBounds(this.getWidth() / 2 - 70, hy + 75, 140, 40);
		this.b_options.setBounds(this.getWidth() / 2 - 60, hy + 45, 120, 25);
		
		((WidgetSplash)this.widgets.get(0)).setPosition(this.getWidth() / 2 + 65, 120);
		super.resizeWidgets();
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int tx = (int)(this.getWidth() / 32) + 2;
		int ty = (int)(this.getHeight() / 32) + 2;
		
		for (int i = 0; i < tx; i++)
		{
			for (int j = 0; j < ty; j++)
			{
				g.drawImage(this.images[0], i * 32, j * 32, null);
			}
		}
		g.drawImage(this.images[1], this.getWidth() / 2 - 256, 25, null);
		
		g.setColor(Color.lightGray);
		tx = this.getWidth() / 2 - 80;
		ty = this.getHeight() / 2 - 80 - 30;
		int w = (this.getWidth() / 2 + 80) - tx;
		int h = 150;
		g.fillRect(tx, ty, w, h);
		
		g.setColor(Color.black);
		g.drawString("Username:", tx + 45, ty + 20);
		
		g.setColor(Color.white);
		tx = 5;
		ty = this.getHeight() - 15;
		g.drawString(LauncherBrand.LAUNCHER_NAME + " v" + LauncherBrand.LAUNCHER_VERSION, tx, ty);
		
		super.paintWidgets(g);
	}
}
