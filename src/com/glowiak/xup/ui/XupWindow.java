package com.glowiak.xup.ui;

import javax.swing.JFrame;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import java.util.ArrayList;

import com.glowiak.xup.launcher.Launcher;
import com.glowiak.xup.launcher.LauncherBrand;
import com.glowiak.xup.ui.widget.WidgetUpdateThread;

@SuppressWarnings("serial")
public class XupWindow extends JFrame
{
	protected XupLoginPanel p_login;
	protected XupOptionsPanel p_options;
	protected XupDownloadPanel p_download;
	
	protected ArrayList<ResizablePanel> update_list;
	private int currentPanel;
	
	public Launcher launcher;
	public Dimension defaultSize;
	
	public XupWindow()
	{
		super(LauncherBrand.WINDOW_TITLE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		this.launcher = new Launcher();
		this.defaultSize = new Dimension(16 * 50, 16 * 30);
		this.setMinimumSize(this.defaultSize);
		
		new Thread(new WidgetUpdateThread(60, this.launcher)).start();
		
		this.p_login = new XupLoginPanel(this);
		this.p_options = new XupOptionsPanel(this);
		this.p_download = new XupDownloadPanel(this);
		
		this.update_list = new ArrayList<ResizablePanel>();
		this.update_list.add(this.p_login);
		this.update_list.add(this.p_options);
		this.update_list.add(this.p_download);
		
		this.setIconImage(this.p_login.images[2]);
		
		this.addComponentListener(new ComponentAdapter()
		{
			public void componentResized(ComponentEvent e)
			{
				for (int i = 0; i < update_list.size(); i++)
				{
					update_list.get(i).updateOnResize();
					update_list.get(i).validate();
					update_list.get(i).updateUI();
					update_list.get(i).repaint();
				}
			}
		});
		
		try {
			this.setPanel(0);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	private final void packComponents()
	{
		this.getContentPane().removeAll();
		this.add(this.update_list.get(this.currentPanel));
		this.pack();
		this.update_list.get(this.currentPanel).updateOnResize();
		this.repaint();
	}
	public final void setPanel(int p) throws IndexOutOfBoundsException
	{
		if (p >= this.update_list.size())
		{
			throw new IndexOutOfBoundsException("Index out of range");
		}
		this.currentPanel = p;
		
		this.packComponents();
	}
}
