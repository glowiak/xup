package com.glowiak.xup.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.Random;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComponent;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.JOptionPane;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import com.glowiak.xup.launcher.Launcher;

import com.glowiak.xup.ui.widget.WidgetJumpingItem;
import com.glowiak.xup.ui.widget.WidgetUpdateThread;

@SuppressWarnings({"serial", "rawtypes", "unused"})
public class XupOptionsPanel extends ResizablePanel
{
	private static final Random rand = new Random();
	
	private int optionsInHeight;
	
	protected JButton b_back;
	protected Object[][] optcontrols;
	
	public XupOptionsPanel(XupWindow parent)
	{
		super(parent);
		
		this.setDoubleBuffered(true);
		this.setPreferredSize(this.parent.defaultSize);
		this.setLayout(null);
		
		this.initComponents();
		this.updateOnResize();
	}
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Image dirt = this.parent.p_login.images[0];
		
		int tx = (int)(this.getWidth() / 32) + 2;
		int ty = (int)(this.getHeight() / 32) + 2;
		
		for (int i = 0; i < tx; i++)
		{
			for (int j = 0; j < ty; j++)
			{
				g.drawImage(dirt, i * 32, j * 32, null);
			}
		}
		
		// draw option names
		int bx = this.getWidth() / 2 - 100 - 145; // 15
		int by = this.getHeight() / 2 - 100 + 15; // 60
		final int spacing = 30;
		
		for (int i = 0; i < this.optcontrols.length; i++)
		{
			g.setColor(Color.white);
			g.drawString((String)this.parent.launcher.opt.options[i][3], bx, by);
			
			by += spacing;
		}
		
		// draw jslider values
		bx = this.getWidth() / 2 - 100 + 250 + 5;
		by = this.getHeight() / 2 - 100 + 15;
		
		for (int i = 0; i < this.optcontrols.length; i++)
		{
			if ((Class)this.optcontrols[i][0] == JSlider.class)
			{
				int val = ((JSlider)this.optcontrols[i][1]).getValue();
				g.drawString("Value: " + val, bx, by);
			}
			
			by += spacing;
		}
		super.paintWidgets(g);
	}
	private final void initComponents()
	{
		this.b_back = new JButton("Done");
		
		this.b_back.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// save
				Launcher l = parent.launcher;
				
				for (int i = 0; i < optcontrols.length; i++)
				{
					Class type = (Class)optcontrols[i][0];
					
					if (type == JCheckBox.class)
					{
						l.opt.options[i][2] = ((JCheckBox)optcontrols[i][1]).isSelected();
					} else if (type == JTextField.class)
					{
						l.opt.options[i][2] = ((JTextField)optcontrols[i][1]).getText();
					} else if (type == JSlider.class)
					{
						l.opt.options[i][2] = ((JSlider)optcontrols[i][1]).getValue();
					}
					WidgetUpdateThread.instance.onOptionsClose();
				}
				if (!l.force_save_opt())
				{
					String er = "Could not save the options!";
					JOptionPane.showMessageDialog(null, er, "Error", JOptionPane.ERROR_MESSAGE);
				}
				
				// return to login panel
				try {
					parent.setPanel(0);
				} catch (IndexOutOfBoundsException ioobe)
				{
					ioobe.printStackTrace();
				}
			}
		});
		
		Launcher l = this.parent.launcher;
		
		this.optcontrols = new Object[l.opt.options.length][2];
		for (int i = 0; i < l.opt.options.length; i++)
		{
			Class clazz;
			JComponent jc;
			
			Class pc = (Class)l.opt.options[i][1];
			if (pc == Boolean.class)
			{
				clazz = JCheckBox.class;
				jc = new JCheckBox();
				
				((JCheckBox)jc).setSelected((Boolean)l.opt.options[i][2]);
			} else if (pc == String.class)
			{
				clazz = JTextField.class;
				jc = new JTextField();
				
				((JTextField)jc).setText((String)l.opt.options[i][2]);
			} else if (pc == Integer.class)
			{
				int val = (Integer)l.opt.options[i][2];
				
				clazz = JSlider.class;
				jc = new JSlider(JSlider.HORIZONTAL, 0, val * 2, val);
				
				((JSlider)jc).addChangeListener(new ChangeListener()
				{
					public void stateChanged(ChangeEvent c)
					{
						repaint();
					}
				});
			} else
			{
				clazz = null;
				jc = null;
			}
			this.optcontrols[i] = new Object[]
			{
					clazz,
					jc
			};
			if (this.optcontrols[i][1] != null)
			{
				if ((Boolean)l.opt.getValue("debug"))
				{
					System.out.println("add " + (String)l.opt.options[i][0]);
				}
				this.add((JComponent)this.optcontrols[i][1]);
			}
		}
		
		this.add(this.b_back);
		
		for (int i = 0; i < rand.nextInt(5) + 3; i++)
		{
			this.widgets.add(new WidgetJumpingItem(this));
		}
	}
	protected final void updateOnResize()
	{
		super.resizeWidgets();
		
		this.optionsInHeight = (int)(this.getHeight() / 45);
		
		this.b_back.setBounds(10, 10, 120, 25);
		
		int bx = this.getWidth() / 2 - 100; // 160
		int by = this.getHeight() / 2 - 100; // 45
		final int spacing = 30;
		for (int i = 0; i < this.optcontrols.length; i++)
		{
			Class type = (Class)this.optcontrols[i][0];
			
			if (type == JCheckBox.class)
			{
				((JCheckBox)this.optcontrols[i][1]).setBounds(bx, by, 25, 25);
			} else if (type == JTextField.class)
			{
				((JTextField)this.optcontrols[i][1]).setBounds(bx, by, 250, 25);
			} else if (type == JSlider.class)
			{
				((JSlider)this.optcontrols[i][1]).setBounds(bx, by, 250, 25);
			}
			by += spacing;
		}
	}
}
