package com.glowiak.xup.ui;

import com.glowiak.xup.ui.widget.Widget;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class ResizablePanel extends JPanel
{
	public XupWindow parent;
	protected ArrayList<Widget> widgets;
	
	public ResizablePanel(XupWindow p)
	{
		super();
		this.parent = p;
		
		this.widgets = new ArrayList<Widget>();
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}
	protected void updateOnResize()
	{
		this.resizeWidgets();
	}
	protected final void paintWidgets(Graphics g)
	{
		for (int i = 0; i < this.widgets.size(); i++)
			this.widgets.get(i).paint(g);
	}
	protected final void resizeWidgets()
	{
		for (int i = 0; i < this.widgets.size(); i++)
			this.widgets.get(i).onResize();
	}
	public final void addWidget(Widget w)
	{
		this.widgets.add(w);
	}
}
